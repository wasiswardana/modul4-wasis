<!DOCTYPE html>
<html lang="en">

<head>
  <title>TP 4 MED</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="css/heroic-features.css" rel="stylesheet">
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</head>

<body>

<form action="update.php" method="post">
      <div class="row text-center">
    <div class="col-12">
     <h4>Profile</h4>
    </div>
   </div>
      <div class="row mb-2">
    <div class="col-sm-3">
     <label>Email</label>
    </div>
    <div class="col-sm-9">
    
    <input type="text" name="username" required placeholder="Username" class="form-control">
     
    </div>
   </div>
   <div class="row mb-3">
    <div class="col-sm-3">
     <label>Username</label>
    </div>
    <div class="col-sm-9">
    <input type="text" name="email" required placeholder="" class="form-control">
    </div>
   </div>
   <div class="row">
    <div class="col-sm-3">
     <label>Mobile Number</label>
    </div>
    <div class="col-sm-9">
     <input type="number" name="mobile_number" required placeholder="Mobile Number" class="form-control" value="<?php echo $hasil['mobile_number'];?>">
    </div>
   </div>

   <div class="row mb-3">
    <div class="col-sm-3">
     <label>Old Password</label>
    </div>
    <div class="col-sm-9">
     <input type="password" name="Password" required placeholder="Old Password" class="form-control">
    </div>
   </div>
   <div class="row mb-3">
    <div class="col-sm-3">
     <label>New Password</label>
    </div>
    <div class="col-sm-9">
     <input type="password" name="confirmPassword" required placeholder="Confirm Password" class="form-control">
    </div>
   </div>
   <div class="row text-center mb-1">
    <div class="col-12">
     <input type="submit"value="update" name=update class="btn btn-info btn-block" class="form-control">
     <a href="landpage.php" class="btn btn-outline-info btn-block">Cancel</a>
    </div>
   </div>
     </form>
   </div>
 </div>

</body>

<style>
    .row {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 300px;
    margin-left: 434px;
}

*, ::after, ::before {
    position: relative;
    left: -10px;
    box-sizing: border-box;
}

.btn-info {
    color: #fff;
    background-color: #438fe0;
    /* border-color: #17a2b8; */
}


</style>

</html>